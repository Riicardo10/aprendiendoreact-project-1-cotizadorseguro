import React, { Component } from 'react';
import Resultado from './Resultado';
import {primeraMayuscula} from '../helper';

class Resumen extends Component {
    mostrarResumen(marca, anio, plan) {
        if( !marca || !anio || !plan ) return null;
        return (
            <div className='resumen'>
                <h2>Resumen de cotizacion</h2>
                <center>
                    <table className='tabla-resumen'>
                        <tbody>
                            <tr>
                                <td> <b> Marca:  </b> </td>
                                <td> {primeraMayuscula(marca)} </td>
                            </tr>
                            <tr>
                                <td> <b> Año:  </b> </td>
                                <td> {anio} </td>
                            </tr>
                            <tr>
                                <td> <b> Plan:  </b> </td>
                                <td> {primeraMayuscula(plan)} </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </div>
        );
    }
    render( ) {
        const {marca, anio, plan} = this.props.detalle;
        const resultado = this.props.resultado;
        return (
            <div>
                {this.mostrarResumen(marca, anio, plan)}
                <Resultado resultado={resultado} />
            </div>
        );
    }
}

export default Resumen;