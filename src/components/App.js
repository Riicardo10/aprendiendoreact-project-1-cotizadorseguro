import React, { Component } from 'react';
import '../styles/App.css';
import {Header} from './Header';
import Formulario from './Formulario';
import {getDiferenciaAnio, getCalculoMarca, getCalculoPlan} from '../helper'
import Resumen from './Resumen';

class App extends Component {

  constructor(){
    super();
    this.state = {
      resultado: '',
      detalle: {}
    }
  }

  cotizarSeguro = (detalle) => {
    const {marca, plan, anio} = detalle;
    let resultado = 2000;
    let diferencia_anio = getDiferenciaAnio( anio );
    resultado -= (diferencia_anio * 3) * resultado / 100;
    resultado = getCalculoMarca( marca ) * resultado;
    resultado = parseFloat( resultado * getCalculoPlan(plan) ).toFixed( 2 );
    this.setState( {
      resultado,
      detalle: {
        marca, plan, anio
      }
    } );
  }

  render() {
    return (
      <div>
        <div className='contenedor-header'>
          <Header 
              titulo='COTIZADOR DE SEGUROS' />
        </div>
        <div className='contenedor-body'>
          <Formulario
              cotizarSeguro={this.cotizarSeguro} />
        </div>
        <div className='contedor-resumen'>
          <Resumen 
              detalle = {this.state.detalle} 
              resultado = {this.state.resultado} />
        </div>
      </div>
    );
  }
}

export default App;
