import React, { Component } from 'react';

class Formulario extends Component {

    constructor(props) {
        super();
        this.marca = React.createRef();
        this.anio = React.createRef();
        this.plan = React.createRef();
    }

    enviarFormulario = ( e ) => {
        e.preventDefault();
        const detalle = {
            marca: this.marca.current.value,
            anio: this.anio.current.value,
            plan: this.plan.current.value,
        }
        this.props.cotizarSeguro( detalle );
        e.currentTarget.reset();
    }
    render() {
        return (
            <form onSubmit={this.enviarFormulario}>
                <center>
                    <table className='formulario'>
                        <tbody>
                            <tr>
                                <td> <label> Marca: </label> </td>
                                <td> 
                                    <select name='marca' ref={this.marca}>
                                        <option value='nissan'>Nissan</option>
                                        <option value='toyota'>Toyota</option>
                                        <option value='ford'>Ford</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr>
                                <td> <label> Año: </label> </td>
                                <td> 
                                    <select name='anio' ref={this.anio}>
                                        <option value='2018'>2018</option>
                                        <option value='2017'>2017</option>
                                        <option value='2016'>2016</option>
                                        <option value='2015'>2015</option>
                                        <option value='2014'>2014</option>
                                        <option value='2013'>2013</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr>
                                <td> <label> Plan: </label> </td>
                                <td> 
                                <select name='plan' ref={this.plan}>
                                        <option value='ninguno'>Ninguno</option>
                                        <option value='basico'>Basico</option>
                                        <option value='completo'>Completo</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input className='btn-cotizar' type='submit' value='Cotizar' />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </form>
        );
    }
}

export default Formulario;