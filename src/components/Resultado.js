import React, { Component } from 'react';

class Resultado extends Component {
    mostrarResultado(resultado) {
        if( !resultado ) return null;
        return <font size='5' > $ {this.props.resultado} </font>
    }
    render() {
        const resultado = this.props.resultado;
        return (
            <div className='resultado'>
                {this.mostrarResultado(resultado)}
            </div>
        );
    }
}

export default Resultado;