export function getDiferenciaAnio( anio ) {
    return new Date().getFullYear() - anio;
}

export function getCalculoMarca( marca ) {
    let incremento = 0;
    switch( marca ) {
        case 'nissan':
            incremento = 1;
        break;
        case 'toyota':
            incremento = 2;
        break;
        case 'ford':
            incremento = 3;
        break;
        default:
        break;
    }
    return incremento;
}

export function getCalculoPlan( plan) {
    if(plan === 'ninguno')
        return 1;
    if(plan === 'basico')
        return 1.5;
    if(plan === 'completo')
        return 2;
}

export function primeraMayuscula( texto ) {
    return texto.charAt(0).toUpperCase() + texto.slice(1);
}